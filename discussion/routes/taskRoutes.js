//contains all the endpoints for our application
//we separate the routes such that 'index.js' only contains information on the server

const express = require('express');

//creates a Router instance that functions as a middleware and routing system
//Allow access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();

const taskController = require('../controllers/taskController');
// Routes
// The routes are responsible for defining the URIs that our client accesses and the corresponding controller function that will be used when a route is accessed
// they invoked the controller functions from the controller files
// all the business logic is done in the controller


//Route to GET ALL THE TASK

router.get('/', (req, res) => {
	taskController.getAllTasks().then(resultFromController => {
		res.send(resultFromController)
	})
})



//Route for CREATING NEW TASK
router.post('/', (req, res) => {

	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})



//Route for DELETING A TASK
router.delete('/:id', (req, res) => {

	console.log(req.params)

	taskController.deleteTask(req.params.id).then(resultFromController => {
		res.send(resultFromController);
	})
})



//Route for UPDATING A TASK
// This route expects to receive a PUT request at the URL "/tasks/:id"
// The whole URL is at "http://localhost:4000/tasks/:id"
router.put("/:id", (req, res) => {

	// The "updateTask" function will accept the following 2 arguments:
			// "req.params.id" retrieves the task ID from the parameter
			// "req.body" retrieves the data of the updates that will be applied to a task from the request's "body" property
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));

});




// [ACTIVITY]
// Route for GETTING SPECIFIC TASK

router.get('/:id', (req, res) => {
	taskController.findOne().then((resultFromController) => {
		res.send(resultFromController);
	})
})


// Route for UPDATING A STATUS
router.put('/:id/complete', (req, res) => {
	taskController.updateStatus(req.params.id, req.body).then((resultFromController) => {
		res.send(resultFromController);
	})
})



//use "module.exports" to export the router object to use in the "index.js"
module.exports = router;
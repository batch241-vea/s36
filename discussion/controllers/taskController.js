// Controller contains the functions and business logic of our Express JS Application
// Meaning, all the operation it can do will be placed in this file.

//uses the "require" directive to allow access to the "Task" model which allows us to access the methods to perform CRUD operations
//allow us to use the contents of the "task.js" file in the models folder

const Task = require('../models/task');

//Controller Function for GETTING ALL THE TASKS
//Defines the functions to be used in the "taskRoutes.js" file and export these functions

module.exports.getAllTasks = () => {

	//the ".then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client/Postman	

	//model.mongoose method
	return Task.find({}).then(result => {

		return result;
	})
}


module.exports.createTask = (requestBody) => {

	//Cretes a task object based in the Mongoose Model "task"
	let newTask = new Task({
		//Sets the "name" property with the value received from the client
		name: requestBody.name
	})
	//the first parameter will store the result return by the Mongoose save method
	//the second parameter will store the "error" object
	return newTask.save().then((task, error) => {

		if (error) {
			console.log(error)
			return false;
		} else {
			return task;
		}
	})
}


module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {

		if(err) {
			console.log(err)
			return false
		} else{
			return removedTask
		}
	})

}



// Controller function for UPDATING A TASK
// The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
// The updates to be applied to the document retrieved from the "req.body" property coming from the client is renamed as "reqBody"
module.exports.updateTask = (taskId, reqBody) => {

	// The "findById" Mongoose method will look for a task with the same id provided from the URL
	// The "return" statement, returns the result of the Mongoose method "findById" back to the "taskRoute.js" file which invokes this function 
	return Task.findById(taskId).then((result, error) => {

		// If an error is encountered returns a "false" boolean back to the client/Postman
		if (error) {
			console.log(error)
			return false
		}

		// Results of the "findById" method will be stored in the "result" parameter
		// It's "name" property will be reassigned the value of the "name" received from the request
		result.name = reqBody.name;

		// Saves the updated object in the MongoDB database
		// The document already exists in the database and was stored in the "result" parameter
		// The "return" statement returns the result of the "save" method to the "then" method chained to the "findById" method which invokes this function
		return result.save().then((updatedTask, saveErr) => {

				// If an error is encountered returns a "false" boolean back to the client/Postman
				if(saveErr) {

					console.log(saveErr)
					return false

				// Update successful, returns the updated task object back to the client/Postman
				} else {

					return updatedTask
				}
		})
	})

}




// [ACTIVITY]

module.exports.findOne = (taskId) => {
	return Task.find({taskId}).then((result) => {
		return result;
	})

}



module.exports.updateStatus = (taskId, reqStatus) => {
	return Task.findById(taskId).then((result, error) => {

		if (error){
			console.log(error)
			return false;
		} else {

			result.status = reqStatus.status;

			return result.save().then((updatedStatus, saveErr) => {
				if (saveErr){
					console.log(saveErr)
				} else{
					return updatedStatus
				}
			})
		}
	})
}
